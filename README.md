# Rundgänge

Ein interaktiver Rundgang durch das Gängeviertel

https://i.gaengeviertel.de


Konzept: Stephan Fender, Lisa Mersmann und Daniel Nürrenbach

Texte und Inhalte: Stephan Fender

Gestaltung und Schilder: Lisa Mersmann

Webdesign und Programmierung: Daniel Nürrenbach

