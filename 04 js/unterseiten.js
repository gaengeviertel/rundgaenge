var sprache

$(document).ready(function() {
  $('.textEN').hide()
  // Der Button wird mit JavaScript erzeugt und vor dem Ende des body eingebunden.
  var back_to_top_button = [
    '<a href="#top" class="back-to-top"><img src="../01 img/backUp.svg"></a>'
  ].join('')
  console.log('hier wird durchgegangen')
  $('body').append(back_to_top_button)

  // Der Button wird ausgeblendet
  $('.back-to-top').hide()

  // Funktion für das Scroll-Verhalten
  $(function() {
    $(window).scroll(function() {
      if ($(this).scrollTop() > 200) {
        // Wenn 200 Pixel gescrolled wurde
        $('.back-to-top').fadeIn()
      } else {
        $('.back-to-top').fadeOut()
      }
    })

    $('.back-to-top').click(function() {
      // Klick auf den Button
      $('body,html').animate(
        {
          scrollTop: 0
        },
        400
      )
      return false
    })

    $('#down').click(function() {
      $('html,body').animate(
        {
          scrollTop: $('#content').offset().top
        },
        'slow'
      )
    })
  }) // Ende Funktion Scroll Effekt
}) //ENDE doc.ready function
