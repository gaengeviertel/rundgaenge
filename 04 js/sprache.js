var sprache
$(document).ready(function() {
  //Click Funktion für Sprachwechsel
  $('#de').click(function() {
    localStorage.setItem('sprache', 'deutsch')
    $('#de').css('background', '#FF2D2D')
    $('#en').css('background', '#d8d8d8')
    sprache = 'deutsch'
    console.log(sprache)
    $('.textDE').show()
    $('.textEN').hide()
  })
  $('#en').click(function() {
    localStorage.setItem('sprache', 'englisch')
    $('#de').css('background', '#d8d8d8')
    $('#en').css('background', '#FF2D2D')
    sprache = 'englisch'
    console.log(sprache)
    $('.textDE').hide()
    $('.textEN').show()
  })
  //Unabhängig vom Click muss die Sprache gespeichert werden!
  if (localStorage.getItem('sprache') == 'englisch') {
    $('#de').css('background', '#d8d8d8')
    $('#en').css('background', '#FF2D2D')
    sprache = 'englisch'
    console.log(sprache)
    $('.textDE').hide()
    $('.textEN').show()
  } else {
    $('#de').css('background', '#FF2D2D')
    $('#en').css('background', '#d8d8d8')
    sprache = 'deutsch'
    console.log(sprache)
    $('.textDE').show()
    $('.textEN').hide()
  }
})
